<?php
// ...

// Récupérer l'identifiant de l'article à partir des données de la requête
$articleId = $_POST['article_id'];

// Vérifier si l'article est déjà marqué comme favori dans le stockage local
$isFavorite = isset($_SESSION['favorite_articles'][$articleId]);

// Si l'article est déjà marqué comme favori, supprimer l'entrée correspondante du stockage local
if ($isFavorite) {
    unset($_SESSION['favorite_articles'][$articleId]);
    $isFavorite = false;
} else {
    // Sinon, ajouter l'article comme favori dans le stockage local
    $_SESSION['favorite_articles'][$articleId] = true;
    $isFavorite = true;
}

// Retourner le statut "is_favorite" mis à jour
echo json_encode(["success" => true, "is_favorite" => $isFavorite]);
