CREATE TABLE users (
    id INT(11) AUTO_INCREMENT NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    lastname VARCHAR(45) NOT NULL,
    birthdate DATE NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    pwd VARCHAR(75) NOT NULL,
    admin TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE articles (
    id INT(11) AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description TEXT NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    image VARCHAR(255),
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE categories (
	id INT(11) AUTO_INCREMENT NOT NULL,
	name VARCHAR(45) NOT NULL,
	PRIMARY KEY (id)
); 

CREATE TABLE article_category (
	article_id INT(11) NOT NULL,
	category_id INT(11) NOT NULL,
	PRIMARY KEY (article_id, category_id),
	FOREIGN KEY (article_id) REFERENCES articles(id) ON DELETE CASCADE,
	FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE
);

CREATE TABLE user_favorite_article (
    article_id INT(11) NOT NULL,
	user_id INT(11) NOT NULL,
	PRIMARY KEY (article_id, user_id),
	FOREIGN KEY (article_id) REFERENCES articles(id) ON DELETE CASCADE,
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);