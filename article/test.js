// Variable qui nous servira à savoir si nous voulons insérer ou mettre à jour un article

let article_id = null;




//! A FAIRE EN DERNIER

//TODO Rendre possible l'affichage uniquement des articles en favoris sur cette page




function addArticle(article) {

    const ctn = $("<article></article>"); // Je crée un élement article

    ctn.addClass("art_box"); // J'ajoute à mon élément article la classe 'art_box'

    ctn.attr("id", "article_" + article.id); // J'ajoute à mon élément article l'id 'article_' + son id




    const title = $("<h2></h2>").text(article.name); // Je crée un élément h2 et je lui ajoute le texte correspondant au nom de mon article




    const desc_ctn = $("<div></div>"); // Je crée le conteneur de l'article et l'image

    const desc = $("<p></p>").text(article.description); // Je crée un élément p et je lui ajoute le texte correspondant à la description de mon article

    desc.addClass("desc"); // J'ajoute la classe desc au paragraphe de la description




    let img; // Je déclare la variable img sans valeur;

    if (article.image) img = $("<img>").attr("src", "../assets/" + article.image); // Je crée une image et j'affecte la source




    const author = $("<p></p>").text(article.author); // Je crée un élément p et je lui ajoute le texte correspondant à l'auteur de mon article




    const created_at = $("<small></small>"); // Je crée un élément small




    let date; // Je crée une variable date qui va contenir la date de création de l'article




    //? Si l'article à une date de création je l'affecte à la variable date

    if (article.created_at) date = new Date(article.created_at);

    //? Sinon je crée une date

    else date = new Date();




    let day = date.getDate(); // J'attribue à une variable la valeur du jour de la date que j'ai créée.

    if (day < 10) day = '0' + day; // Si le jour est inférieur à 10 je rajoute un 0 devant




    let month = date.getMonth() + 1; // J'attribue à une variable la valeur du mois de la date que j'ai créée. J'ajoute +1 car le mois commence à 0

    if (month < 10) month = '0' + month; // Si le mois est inférieur à 10 je rajoute un 0 devant




    let hours = date.getHours(); // J'attribue à une variable la valeur des heures de la date que j'ai créée.

    if (hours < 10) hours = '0' + hours; // Si l'heure est inférieur à 10 je rajoute un 0 devant




    let min = date.getMinutes(); // J'attribue à une variable la valeur ddes minutes de la date que j'ai créée.

    if (min < 10) min = '0' + min; // Si les minutes sont inférieurs à 10 je rajoute un 0 devant




    // J'attribue à l'element small le texte correspondant à la date de création de mon article au format jj/mm/yyyy hh:mm

    created_at.text(day + "/" + month + "/" + date.getFullYear() + " " + hours + "h" + min);




    const artcat_ctn = $("<div></div>");

    artcat_ctn.addClass("artcat_ctn");

    

    article.categories.forEach(cat => {

        const cat_btn = $("<button></button>");

        cat_btn.prop("disabled", true);

        cat_btn.addClass("btn regular cat_btn");

        cat_btn.text(cat.name);

        

        artcat_ctn.append(cat_btn)

    });

    

    const bottom_ctn = $("<div></div>"); // Je crée un conteneur où placer l'auteur, la date, les boutons et les catégories

    bottom_ctn.append(artcat_ctn, author, created_at);




    //? Si mon id utilisateur est le même que celui de l'auteur de l'article OU que je n'ai pas de date de création dans l'article alors

    if (user.id == article.user_id || !article.created_at) {

        const delete_btn = $("<button></button>"); // Je crée un bouton supprimer

        delete_btn.html("<i class='fa fa-trash'></i>"); // J'ajoute une icone de poubelle à mon bouton

        delete_btn.addClass("btn salmon action_btn"); // J'ajoute du style à mon bouton




        // J'ajoute un écouteur d'événement clic sur le bouton supprimer

        delete_btn.click(() => {

            //? Si confirmation de l'utilisateur alors j'appelle la fonction deleteArticle

            if (confirm("Êtes-vous sur de vouloir supprimer cet article ?")) deleteArticle(article.id);

        });




        const update_btn = $("<button></button>"); // Je crée un bouton modifier

        update_btn.html("<i class='fa fa-pencil'></i>"); // J'ajoute une icone de stylo à mon bouton

        update_btn.addClass("btn ocean action_btn"); // J'ajoute du style à mon bouton




        // J'ajoute un écouteur d'événement clic sur le bouton modifier

        update_btn.click(() => {

            // Je modifie le titre de la box du fomulaire

            $(".box h1").text("Modifier un article");




            // J'affecte à la variable définit en haut du fichier la valeur de l'id de l'article sur lequel on a cliqué

            article_id = article.id;




            // Je préremplie mes champs de formulaire

            $("#name").val(article.name);

            $("#desc").val(article.description);




            // Je parcours les id des catégories de l'article

            article.categories.forEach(({id}) => {

                // Je coche les checkbox associées à l'article

                $(":checkbox[value=" + id + "]").prop("checked", true);

            });




            $(".box").addClass("open"); // J'affiche mon formulaire

            $("#overlay").addClass("open"); // J'affiche mon overlay

        });

        

        

        // J'ajoute mes boutons au conteneur des boutons

        bottom_ctn.append(update_btn, delete_btn);

    }

    

    //TODO Sur chaque article ajouter un bouton comme supprimer ou mettre à jour

    //TODO L'icone de ce bouton si l'article n'est pas favori est la suivante: star-o

    //TODO L'icone de ce bouton si l'article est favori est la suivante: star

    //TODO Les classes à ajouter à ce bouton sont les suivante: btn art_btn favorite




    //TODO Au clic du bouton précédent ajouter ou supprimer l'article en favori en fonction de son état actuel dans la bdd

    //TODO Une fois le favori enregistré ou supprimé, mettre à jour l'icone




    // J'ajoute les éléments description et image dans un conteneur

    desc_ctn.append(desc, img);

    const favori_btn = $("<button></button>");

        favori_btn.html("<i class='fa fa-star-o'></i>");

        favori_btn.addClass("btn art_btn favorite");

        favori_btn.click(() => {

            favori_btn.html("<i class='fa fa-star'></i>");

        

        });

    ctn.append(title, desc_ctn, bottom_ctn,favori_btn); // J'ajoute les éléments titre, description avec image, auteur, date de création et les boutons dans mon article




    //? Si j'ai une date de création de l'article alors c'est un article existant je l'ajoute dans la liste

    if (article.created_at) $("#art_ctn").append(ctn);

    //? Sinon c'est un nouvel article je l'ajoute au début de la liste

    else $("#art_ctn").prepend(ctn);

}




/**

* @desc Fait appel au php pour supprimer un article

* @param string id - Contient l'id de l'article

* @return void - Ne retourne rien

*/

function deleteArticle(id) {

    $.ajax({

        url: "../php/article.php",

        type: "POST",

        dataType: "json",

        data: {

            choice: "delete",

            id

        },

        success: (res) => {

            //? Si la réponse est un succès alors je supprime l'article HTML correspondant

            if (res.success) $("#article_" + id).remove();

            else alert(res.error);

        }

    });

}




/**

* @desc Fait appel au php pour inserer un article

* @param string name - Contient le nom de l'article

* @param string desc - Contient la description de l'article

* @return void - Ne retourne rien

*/

function insertArticle(name, desc, picture, cat_ids) {

    // Je crée une nouvelle instance de FormData afin d'ajouter chaque de mes clés

    const fd = new FormData();

    fd.append("choice", "insert");

    fd.append("name", name);

    fd.append("desc", desc);

    fd.append("cat_ids", JSON.stringify(cat_ids));

    fd.append("picture", picture);




    $.ajax({

        url: "../php/article.php",

        type: "POST",

        dataType: "json",

        contentType: false,

        processData: false,

        cache: false,

        data: fd,

        success: (res) => {

            if (res.success) {

                // J'appelle la fonction addArticle afin de créer un article par rapport aux données du formulaire

                addArticle({

                    id: res.id,

                    name,

                    description: desc,

                    author: user.firstname + " " + user.lastname,

                    image: res.image,

                    categories: res.categories

                });




                $(".box").removeClass("open"); // Je cache mon formulaire

                $("#overlay").removeClass("open"); // Je cache mon overlay

            } else alert(res.error);

        }

    });

}




/**

* @desc Fait appel au php pour mettre à jour un article

* @param string name - Contient le nom de l'article

* @param string desc - Contient la description de l'article

* @return void - Ne retourne rien

*/

function updateArticle(name, desc, picture, cat_ids) {

    const fd = new FormData();

    fd.append("choice", "update");

    fd.append("name", name);

    fd.append("desc", desc);

    fd.append("picture", picture);

    fd.append("cat_ids", JSON.stringify(cat_ids));

    fd.append("id", article_id);




    $.ajax({

        url: "../php/article.php",

        type: "POST",

        dataType: "json",

        contentType: false,

        processData: false,

        cache: false,

        data: fd,

        success: (res) => {

            //? Si la réponse renvoie un succès alors

            if (res.success) {

                //* Je modifie chacune des nouvelles données

                $("#article_" + article_id + " h2").text(name);

                $("#article_" + article_id + " p.desc").text(desc);

                $("#article_" + article_id + " img").attr("src", "../assets/" + res.image);

            

                $("#article_" + article_id + " .artcat_ctn").html("");

                res.categories.forEach(cat => {




                    const cat_btn = $("<button></button>");

                    cat_btn.prop("disabled", true);

                    cat_btn.addClass("btn regular cat_btn");

                    cat_btn.text(cat.name);

            

                    $("#article_" + article_id + " .artcat_ctn").append(cat_btn)

                });

            }




            // Je défini mon article_id à null, ma modification est terminée

            article_id = null;




            $(".box").removeClass("open"); // Je cache mon formulaire

            $("#overlay").removeClass("open"); // Je cache mon overlay

        }

    });

}




// J'effectue un appel AJAX pour récupérer tous les articles

$.ajax({

    url: "../php/article.php", // URL cible

    type: "GET", // Type de méthode de requête HTTP

    dataType: "json", // Type de réponse attendue

    data: { // Donnée(s) à envoyer s'il y en a

        choice: "select"

    },

    success: (res) => {

        //? Si la réponse est un succès alors

        if (res.success) {

            // J'itère sur les articles que je récupère

            res.articles.forEach(art => {

                // J'appelle la fonction addArticle afin de créer un article par élement de mon tableau articles

                addArticle(art);

            });

        } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur

    }

});




$("form").submit(event => {

    event.preventDefault(); // Je préviens le comportement par défaut du formulaire pour empecher la page de se recharger




    const cat_ids = [];

    $("input:checkbox:checked").each(function () {

        cat_ids.push($(this).val());

    });




    // Je récupère dans des variables les valeurs des champs de mon formulaire à la soumission

    const name = $("#name").val();

    const desc = $("#desc").val();

    const picture = $("#picture")[0].files[0];




    //? Si j'ai une valeur dans la variable article_id alors je mets à jour. Sinon j'insère.

    if (article_id) updateArticle(name, desc, picture, cat_ids);

    else insertArticle(name, desc, picture, cat_ids);

});




// Au clic de la div "Ajouter un article"

$("header div.btn").click(() => {

    $(".box h1").text("Ajouter un article"); // Je mets à jour le titre du formulaire




    document.querySelector("form").reset(); // Je vide le formulaire




    $(".box").addClass("open"); // J'affiche mon formulaire

    $("#overlay").addClass("open"); // J'affiche mon overlay

});




// Au clic de l'overlay

$("#overlay").click(() => {

    $(".box").removeClass("open"); // Je cache mon formulaire

    $("#overlay").removeClass("open"); // Je cache mon overlay

});




// Je récupère les données de toutes mes catégories

$.ajax({

    url: "../php/category.php",

    type: "GET",

    dataType: "json",

    data: {

        choice: "select"

    },

    success: (res) => {

        if (res.success) {

            // J'itère sur les catégories que je reçois

            res.categories.forEach(cat => {

                // Pour chaque catégorie je crée une div qui contiendra un input checkbox et un label

                const checkDiv = $("<div></div>");

                checkDiv.addClass("checkDiv");




                //! Les attributs de l'input sont très important

                const checkInput = $("<input>");

                checkInput.attr("type", "checkbox");

                checkInput.attr("name", "categories");

                checkInput.val(cat.id);




                const label = $("<label></label>").text(cat.name);




                checkDiv.append(checkInput, label)

                $("#cat_ctn").append(checkDiv); // J'ajoute ma div à mon form

            });

        } else alert(res.error);

    }

});

